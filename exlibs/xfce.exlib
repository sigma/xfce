# Copyright (c) 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# require xfce [ module="xfce" suffix="tar.bz2" intltoolize="false" \
#                require_dev_tools="false" autotools="false" ]

myexparam pn=${PN}
myexparam pv=${PV}
myexparam pnv=$(exparam pn)-$(exparam pv)
myexparam module=xfce
myexparam suffix=tar.bz2
myexparam -b intltoolize=true
myexparam -b require_dev_tools=false
myexparam -b autotools=false

exparam -b autotools && require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

dl_pn=$(exparam pn)
dl_pn=${dl_pn,,}

HOMEPAGE="https://www.xfce.org"

REMOTE_IDS="xfce:$(exparam module)/$(exparam pn)"

if ever is_scm; then
    # Prefer the GitHub mirror since it provides an https transport
    SCM_REPOSITORY="https://github.com/xfce-mirror/$(exparam pn).git"
    require scm-git
else
    DOWNLOADS="https://archive.xfce.org/src/$(exparam module)/${dl_pn}/$(ever range 1-2)/$(exparam pnv).$(exparam suffix)"
fi

LICENCES="GPL-2"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
"

if exparam -b require_dev_tools;then
    if ! exparam -b autotools;then
        die "require_dev_tools isn't needed if autotools isn't enabled."
    fi
    DEPENDENCIES+="
        build:
            xfce-extra/xfce4-dev-tools
    "
    AT_M4DIR=( ${AT_M4DIR[@]} /usr/share/xfce4/dev-tools/m4macros )
fi

export_exlib_phases src_prepare

xfce_src_prepare() {
    [[ -d "${WORK}/m4" ]] || edo mkdir "${WORK}/m4"
    exparam -b autotools && autotools_src_prepare || default
    if exparam -b intltoolize && exparam -b autotools;then
        edo intltoolize --automake --force --copy
    elif ! exparam -b autotools && exparam -b intltoolize;then
        edo intltoolize --force --copy
    fi
}

